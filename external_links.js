define(function(){
	var ExternalLinks = {
		init : function(){
			document.addEventListener('deviceready', ExternalLinks.onDeviceReady, false);
		},
		onDeviceReady : function(){
			if (!window.device) {
				window.device = { platform: 'Browser' };
			}
		},

		hijackLinks : function(){
			var links = document.getElementsByTagName('a');
			for(var i=0; i<links.length; i++){
				if (device.platform.toUpperCase() === 'IOS' || device.platform.toUpperCase() === 'ANDROID') {
					links[i].addEventListener('click', ExternalLinks.handleExternalURL, false);
				}
			}
		},

		handleURL : function(e){
			var source = e.target;
			while(source.tagName.toUpperCase() !== 'A') source = source.parentNode;
			var url = source.href;
			ExternalLinks.openURL(url);
			e.preventDefault();
		},

		openURL : function(url){
			if(!window.device || window.device.platform.toUpperCase() === 'BROWSER'){
				ExternalLinks.handleBrowser(url);
			}else if (device.platform.toUpperCase() === 'IOS') {
				ExternalLinks.handleIOS(url);
			}else if(device.platform.toUpperCase() === 'ANDROID'){
				ExternalLinks.handleAndroid(url);
			}
		},

		handleBrowser : function(url){
			window.open(url, '_system');
		},
		handleIOS : function(url){
			window.open(url, '_system');
		},
		handleAndroid : function(url){
			navigator.app.loadUrl(url, { openExternal: true });
		}
	};

	return ExternalLinks;
});